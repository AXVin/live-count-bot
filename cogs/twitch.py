import discord
from discord.ext import commands

from rethinkdb import RethinkDB
import aiohttp

from .utils import db, config
from .utils import twitch_utils as tu


class Twitch(commands.Cog):
    """Twitch Live Follower and Views Count!! Check out `l!faq twitch` for how to setup!"""

    def __init__(self, bot):
        self.bot = bot



    @commands.group(invoke_without_command=True)
    async def twitch(self, ctx):
        """Commands for fetching twitch analytics manually!"""
        await ctx.send_help(ctx.command)


    @twitch.command()
    async def followers(self, ctx, streamer:str):
        """Manually fetches the follower count of the `streamer`"""
        streamer = streamer.lower()
        guild = ctx.guild
        await db.check_default_guild_settings(guild)
        client_id = await tu.get_twitch_client_id(guild)
        streamerdata = await tu.get_twitch_streamer(ctx, client_id, streamer)
        streamer_id = streamerdata['id']

        followers = await tu.get_twitch_followers(ctx.session,
                                                  client_id,
                                                  streamer_id)

        await ctx.send(f'**{streamerdata["display_name"]}** '
                       f'has {followers:,d} followers!')
        # r, connection = await ctx.acquire()
        async with ctx.acquire():
            await ctx.r.table('twitch'
            ).get(streamerdata['id']
            ).update({'followers': followers}
            ).run(ctx.connection)



    @twitch.command()
    async def views(self, ctx, streamer:str):
        """Manually fetches the view count of the `streamer`"""
        streamer = streamer.lower()
        guild = ctx.guild
        await db.check_default_guild_settings(guild)
        client_id = await tu.get_twitch_client_id(guild)
        streamerdata = await tu.get_twitch_streamer(ctx, client_id, streamer)

        views = await tu.get_twitch_views(ctx.session,
                                          client_id,
                                          streamer)

        await ctx.send(f'**{streamerdata["display_name"]}** '
                       f'has {views:,d} views!')

        # r, connection = await ctx.acquire()
        async with ctx.acquire():
            await ctx.r.table('twitch'
            ).get(streamerdata['id']
            ).update({'views': views}
            ).run(ctx.connection)




def setup(bot):
    cog = Twitch(bot)
    bot.add_cog(cog)
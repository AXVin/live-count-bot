import os
import json
import discord

PATH = 'config.json'
with open(PATH, encoding='utf-8', mode="r") as file:
    config = json.load(file)
token = os.environ.get("TOKEN", config["TOKEN"])
dbl_token = os.environ.get("DBL_TOKEN", config["DBL_TOKEN"])
twitch_token = os.environ.get('TWITCH_TOKEN', config['TWITCH_TOKEN'])

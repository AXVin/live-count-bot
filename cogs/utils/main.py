import random
import aiohttp

import discord
from discord.ext import commands



# ==== Custom Errors ==== #

class InvalidSecret(commands.CommandError):
    pass

class MissingSecret(commands.CommandError):
    pass


# ==== Global utils ==== #


def is_int(string:str):
    '''
    Checks if the string is an integer

    Parameters:
    -----------
    string: str
        The string which is to be checked

    Returns:
    -----------
    result: bool
        True if it's an int otherwise False
    '''
    try:
        int(string)
    except ValueError:
        return False
    else:
        return True


def get_random_color():
    '''
    Gets a random discord.Color object

    Returns:
    -----------
    color: discord.Color
        The randomly generated color
    '''
    color = ''.join([random.choice('0123456789ABCDEF') for x in range(6)])
    color = int(color, 16)
    color = discord.Color(value=color)
    return color


async def have_changed(*, keys, before, after):
    '''
    Sees if `values` in `before` and `after` have changed!

    Parameters:
    -----------
    values: seq(Union[list, tuple, set])
        A seq of keys whose values will be matched!
    before: :class:`dict`
        A dictionary of before values
    after: :class:`dict`
        A dictionary of after values

    Returns:
    -----------
    bool:
        True if values changed
    '''
    # Cleanup the unwanted keys!
    for key in before.keys():
        if not key in keys:
            del before[key]

    for key in after.keys():
        if not key in keys:
            del after[key]

    if before==after:
        return None

    changes = []
    for key in before.keys():
        if not before[key] == after[key]:
            changes.append(key)

    # if len(changes) == 1:
    #     return changes[0]
    return changes





async def wait_for_reaction(ctx, author:discord.Member, message:discord.Message, emojis, timeout=None):
    """
    Yup! I can't live without my old wait_for_reactions.
    This one require you to specify timeout as a float
    emojis must be a list of emoji objects
    """
    
    
    bot = ctx.bot
    emoji_str = [str(emoji) for emoji in emojis]
    
    def check(reaction, user):
        if user.bot:
            return False
        if not user==author:
            return False
        if not str(reaction) in emoji_str:
            return False

        return True

#    self = cls(bot)
    try:
        reaction, _ = await bot.wait_for('reaction_add', timeout=timeout, check=check)
    except:    # Shouldn't be silencing all errors but hey, i will still return none!
        await author.send('Timed out!')    # Should be doing it in on_error i believe
        return None
    else:
        return reaction



async def wait_for_message(ctx, author:discord.Member, timeout=None):
    
    
    bot = ctx.bot
    def check(message):
        return message.author==author

    try:
        message = await bot.wait_for('message', timeout=timeout, check=check)
    except:
        await author.send("Timed out!")
        return None
    else:
        return message



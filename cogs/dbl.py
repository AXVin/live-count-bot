import discord
from discord.ext import commands, tasks

import aiohttp
import asyncio
import logging
from datetime import datetime

from cogs.utils import config



class DBL(commands.Cog):
    """Cog for posting bot information to DBL"""
    
    def __init__(self, bot):
        self.bot = bot
        self.token = config.dbl_token
        self.base_url = "https://discordbots.org/api"
        self.headers = {"Authorization": self.token}
        self.update_stats.start()


    def cog_unload(self):
        self.update_stats.stop()


    @tasks.loop(minutes=10)
    async def update_stats(self):
        """This will run every 5 minutes to update server count!"""

        total_servers = len(self.bot.guilds)
        end_url = f"/bots/{self.bot.user.id}/stats"
        body = {"server_count": total_servers}
        async with self.bot.session.post(self.base_url+end_url, headers=self.headers, data=body) as response:
            pass
        print(f"{datetime.utcnow()} Posted Server Count! Total Servers:- {total_servers}")


    @update_stats.before_loop
    async def before_update_stats(self):
        if not self.token:
            self.update_stats.stop()
        await self.bot.wait_until_ready()


    @commands.command(name="dbl-update")
    @commands.is_owner()
    async def dbl_update(self, ctx):
        """Command used to manually update server count on DBL!"""

        end_url = f"/bots/{self.bot.user.id}/stats"
        total_servers = len(self.bot.guilds)
        body = {"server_count": total_servers}
        async with self.bot.session.post(self.base_url+end_url, headers=self.headers, data=body) as response:
            text = await response.text()
        embed = discord.Embed(title="Posted Server Count!", description=f"```json\n{text}\n```", color=discord.Color.teal())
        await ctx.send(embed=embed)



    @commands.command(name="upvote-check")
    @commands.is_owner()
    async def upvote_check(self, ctx, *, user:discord.User):
        """Checks if the user has upvoted or not!"""
        
        end_url = f"/bots/{self.bot.user.id}/check"
        params = {"userId": user.id}
        async with self.bot.session.get(self.base_url+end_url, params=params, headers=self.headers) as response:
            data = await response.json()
        
        message = f"{user.mention} [{user.id}] has upvoted today!" if data["voted"] else f"{user.mention} [{user.id}] has not upvoted yet!"
        color = discord.Color.green() if data["voted"] else discord.Color.red()
        embed = discord.Embed(description=message, color=color)
        await ctx.send(embed=embed)



def setup(bot):
    n = DBL(bot)
    bot.add_cog(n)

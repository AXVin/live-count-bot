# Streamer check in db.py #
Put the streamer check in cog.utils.db.py because it comes under the
category of database defaults eventhough it involves making API calls

## How will the live counts work? ##
We run a script for each platform(twitch and youtube will be free)
Those scripts will loop over their respecive table in the database and will
query the repective API for data and if it is different than the previously
saved data, it will update the database with the new information.

On the bot side, there will be cog with a background task running which will
hear all the changes through the RethinkDb changefeed. If it recieves one,
it will try to update the connected voice channels with the data recieved!

## How will the secrets work? ##
Each server will require to set the secrets for the respective services
The saved secrets will be used for further commands and live counts for the
server.
For manual commands in DMs, the secrets set in `config` will be used

## Table Relations ##
There will be one Table for guild specific setting contains information
about **secrets** and **connected streamers** to channels. It will look
like this:
```json
"connections": {
	"twitch": [{
		"channel": CHANNEL_ID,
		"type": "followers/views",
		"streamer_id": STREAMER_ID
	}],
	"youtube": [{
		"channel": CHANNEL_ID,
		"type": "subscribers/views/comments/videos"
	}]
}
```

There will also be each Table for different platforms.
The document in each table will contain the **information** regarding
the platform and a **connections** key containing a dict of connected
channels and their type. It looks like this:
```json
"connections": [{
		"channel": CHANNEL_ID, 
		"type": "followers", // Could be anything according to the platform
		"guild": GUILD_ID,
		"format": "Followers: {count}" 
}]
```
Allowed parameters for format are:

| Parameter | Description |
| --------- | ----------- |
| count | Number of "type" |
| name | login name of the creator |
| display_name | Display Name of the creator |
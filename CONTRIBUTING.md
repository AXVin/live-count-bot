# Contributing

We're happy you want to contribute! You can help us in different ways:
* [Open an issue](https://bitbucket.org/AXVin/live-count-bot/issues) with suggestions for improvements
* Fork this repository and submit a pull request
* Improve the [documentation](https://bitbucket.org/AXVin/live-count-bot/wiki)

To submit a pull request, fork the [Live Count Bot repository](https://bitbucket.org/AXVin/live-count-bot) and then clone your fork:
```bash
git clone https://bitbucket.org/<your-username>/live-count-bot.git
```
Make your suggested changes, `git push` and then [submit a pull request](https://bitbucket.org/AXVin/live-count-bot/compare)

# Resources
Some useful resources to get started:
* [Learn Python](https://automatetheboringstuff.com)
* Introduction to [RethinkDB Python Driver](https://rethinkdb.com/docs)
* [Asynchronous](htttps://github.com/rethinkdb/rethinkdb-python) use of rethinkdb python driver
* [Discord.py](https://discordpy.readthedocs.io) API Wrapper
* [Python Style Guide](https://bitbucket.org/AXVin/live-count-bot/STYLE.md) for this project